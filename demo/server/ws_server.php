<?php 

$server = new swoole_websocket_server("0.0.0.0", 8812);


/**监听websocket连接打开事件*/
$server->on('open', function (swoole_websocket_server $server, $request) {
    echo "server: handshake success with fd{$request->fd}\n";
});

function onOpen($server,$request){
    print_r($request->fd);
}

/**监听ws消息事件 */
$server->on('message', function (swoole_websocket_server $server, $frame) {
    echo "receive from {$frame->fd}:{$frame->data},opcode:{$frame->opcode},fin:{$frame->finish}\n";
    $server->push($frame->fd, "yq-push-success");
});

$server->on('close', function ($ser, $fd) {
    echo "client {$fd} closed\n";
});

$server->start();
