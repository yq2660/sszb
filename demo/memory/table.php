<?php

 $table =  new swoole_table(1024);

 $table->column('id',$table::TYPE_INT,4);
 $table->column('name',$table::TYPE_STRING,64);
 $table->column('age',$table::TYPE_INT,4);
 $table->create();

 $table->set('singwa_imooc',['id'=>1,'name'=>'yq','age'=>20]);

 $table['singwa_imooc2'] = [
    'id'=>1,
    'name'=>'yq',
    'age'=>31,
 ];

 $table->del('singwa_imooc2');
 print_r($table['singwa_imooc2']);